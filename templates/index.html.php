<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Audio Converter</title>
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css">
        <style>
            .button {
                border: 1px solid #146094;
                background: #146094;
                transition: background-color 350ms linear;
                padding: 8px 12px;
                color: #FEFEFE;
                text-decoration: none;
                height: 2rem;
                line-height: 2rem;
            }

            .button:hover, .button:active {
                background: #0f537e;
                transition: background-color 350ms linear;
            }

            body {
                margin: 0;
                background: #EFEFEF;
                font-family: "Roboto Thin", sans-serif;
            }

            header {
                height: 3rem;
            }

            .wrapper {
                display: flex;
                min-width: 900px;
                margin: 0;
            }

            .status {
                width: 60px;
                border-top: 1px solid #CCC;
                border-bottom: 1px solid #CCC;
                text-align: center;
            }

            .status > div {
                padding: 6px 0;
                line-height: 2rem;
            }

            .input, .output {
                flex: 1;
                border: 1px solid #CCC;
            }

            .input ul, .output ul {
                list-style: none;
                padding: 0;
                margin: 0;
            }

            .input ul li, .output ul li {
                line-height: 2rem;
                padding: 6px 1rem;
                display: flex;
            }

            .input ul li:hover,
            .input ul li.hover,
            .output ul li.hover,
            .output ul li:hover {
                background: rgba(180, 180, 180, .25);
            }

            .status .check.hover {
                background: rgb(158, 233, 146);
            }

            .status .hover {
                background: rgba(180, 180, 180, .25);
            }

            .input .play, .output .play {
               margin-right: 6px;
            }
        </style>
        <script>
            const runConverter = function() {
                const xhr = new XMLHttpRequest();
                xhr.previous_text = '';
                xhr.onreadystatechange = function() {
                    try {
                        if (xhr.readyState > 2) {
                            const response = xhr.responseText.substring(xhr.previous_text.length);
                            xhr.previous_text = xhr.responseText;

                            console.log(response);
                        }
                    } catch (e) {
                        console.error(e);
                    }
                };

                xhr.open('GET', '/start');
                xhr.send();

                // const request = new Request('/start', {});
                //
                // fetch(request)
                //     .then(function(response) {
                //         console.log(response);
                //     })
                // ;
            };

            document.addEventListener("DOMContentLoaded", function() {
                document.querySelectorAll('[data-hash]').forEach(function(elm) {
                    elm.addEventListener('mouseover', function() {
                        const hash = this.getAttribute('data-hash');

                        document.querySelectorAll('[data-hash]').forEach(function(subElm) {
                            subElm.classList.remove('hover');
                            if (subElm.getAttribute('data-hash') === hash) {
                                subElm.classList.add('hover');
                            }
                        });
                    });
                });
            });
        </script>
    </head>
    <body>
        <header>
            <a class="button" href="javascript:runConverter();">Convert</a>
        </header>
        <main class="wrapper">
            <div class="input">
                <ul>
                    <?php foreach ($inputFiles as $hash => $file) { ?>
                        <li data-hash="<?= $hash ?>">
                            <div>
                                <?= $file['fullname'] ?>
                            </div>
                        </li>
                    <?php } ?>
                </ul>
            </div>
            <div class="status">
                <?php foreach ($inputFiles as $hash => $file) { ?>
                    <div class="<?= isset($outputFiles[$hash]) ? 'check' : 'waiting' ?>" data-hash="<?= $hash ?>">
                        <?php if (isset($outputFiles[$hash])) { ?>
                            <i class="fas fa-check"></i>
                        <?php } else { ?>
                            &nbsp;
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
            <div class="output">
                <ul>
                    <?php foreach ($inputFiles as $hash => $file) { ?>
                        <li data-hash="<?= $hash ?>">
                            <?php if (isset($outputFiles[$hash])) { ?>
                                <div>
                                    <a class="play"><i class="fas fa-play"></i></a>
                                </div>
                                <div>
                                    <?= $outputFiles[$hash]['fullname'] ?>
                                </div>
                                <?php unset($outputFiles[$hash]) ?>
                            <?php } else { ?>
                                <div>&nbsp;</div>
                            <?php } ?>
                        </li>
                    <?php } ?>
                    <?php foreach ($outputFiles as $hash => $file) { ?>
                        <li data-hash="<?= $hash ?>">
                            <div>
                                <a class="play"><i class="fas fa-play"></i></a>
                            </div>
                            <div>
                                <?= $file['fullname'] ?>
                            </div>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        </main>
    </body>
</html>