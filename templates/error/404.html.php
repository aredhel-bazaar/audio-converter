<html>
    <head>
        <meta charset="UTF-8">
        <title>Not found</title>
    </head>
    <body>
        <h1>Not found</h1>
        <p>
            The page you requested was not found.
        </p>

        <a href="/">Back to home</a>
    </body>
</html>