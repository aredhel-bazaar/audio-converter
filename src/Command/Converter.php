<?php
/**
 * Created by PhpStorm.
 * User: Aredhel
 * Date: 31/05/2017
 * Time: 21:05
 */

namespace AudioConverter\Command;

use AudioConverter\Extension\FFMpeg\Format\Audio\Opus;
use AudioConverter\Extension\FFMpeg\Format\Audio\Aac;
use Exception;
use FFMpeg\FFMpeg;
use FFMpeg\Format\Audio\Flac;
use FFMpeg\Format\Audio\Mp3;
use FFMpeg\Format\Audio\Vorbis;
use FFMpeg\Format\Audio\Wav;
use FFMpeg\Media\Audio;
use FFMpeg\Media\Video;

class Converter
{
    /**
     * @var FFMpeg
     */
    private $FFMpeg;
    private $files;
    private $root_dir;
    private $output_dir;
    private $input_dir;
    private $output_format;
    private $quality;

    private $last_percentage_length;
    private $max_file_length = 0;

    private $shortopts = "o:i:";
    private $longopts  = [
        'no-cleanup',
        'output:',
        'input:',
        'format:',
        'quality:',
        'help'
    ];
    private $config    = [
        'cleanUpOutputDir' => true
    ];
    private $logFile;

    const LOG_LEVEL_DEBUG   = 'DEBUG';
    const LOG_LEVEL_NOTICE  = 'NOTICE';
    const LOG_LEVEL_WARNING = 'WARNING';
    const LOG_LEVEL_ERROR   = 'ERROR';
    const LOG_LEVEL_FATAL   = 'FATAL';

    const STATE_SKIPPED  = 0;
    const STATE_PROGRESS = 1;
    const STATE_OK       = 2;
    const STATE_ERROR    = 3;

    const OUTPUT_EXTS = [
        'mp3'  => 'mp3',
        'ogg'  => 'ogg',
        'oga'  => 'oga',
        'aac'  => 'aac',
        'opus' => 'opus',
        'flac' => 'flac',
        'wav'  => 'wav'
    ];


    /**
     * Converter constructor.
     * @throws Exception
     */
    public function __construct()
    {
        if (isset(getopt($this->shortopts, $this->longopts)['help'])) {
            echo "Available options : \n\t--".implode("\n\t--", array_map(function($opt) { return (substr($opt, -1) === ':' ? substr($opt, 0, -1) : $opt); }, $this->longopts)).PHP_EOL;
            exit;
        }

        $this->init();
        $this->configure();

        if ($this->config['cleanUpOutputDir']) {
            $this->cleanOutputDir();
        }

        $this->discoverFiles();
        if (!empty($this->files)) {
            $this->doProcess();
        }

        $this->quit();
    }


    /**
     * @throws Exception
     */
    private function init()
    {
        $this->writeln("Initializing...");

        switch (true) {
            case mb_stristr(PHP_OS, 'DARWIN'):
                $binaries = [
                    'ffmpeg.binaries'  => realpath(__DIR__ . '/../../bin/Mac') . '/ffmpeg',
                    'ffprobe.binaries' => realpath(__DIR__ . '/../../bin/Mac') . '/ffprobe'
                ];
                break;

            case mb_stristr(PHP_OS, 'WIN'):
                $binaries = [
                    'ffmpeg.binaries'  => realpath(__DIR__ . '\..\..\bin\Win64') . '\ffmpeg.exe',
                    'ffprobe.binaries' => realpath(__DIR__ . '\..\..\bin\Win64') . '\ffprobe.exe'
                ];
                break;

            case mb_stristr(PHP_OS, 'LINUX'):
                $binaries = [
                    'ffmpeg.binaries'  => '/usr/bin/ffmpeg',
                    'ffprobe.binaries' => '/usr/bin/ffprobe',
                ];
                break;

            default:
                throw new Exception('OS unknown or not supported');
        }

        $this->FFMpeg        = FFMpeg::create($binaries);
        $this->root_dir      = realpath(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR);
        $this->input_dir     = realpath($this->root_dir . DIRECTORY_SEPARATOR . 'Resources' . DIRECTORY_SEPARATOR . 'input');
        $this->output_dir    = realpath($this->root_dir . DIRECTORY_SEPARATOR . 'Resources' . DIRECTORY_SEPARATOR . 'output');
        $this->output_format = 'mp3';
        $this->logFile       = fopen($this->root_dir . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . date('Y-m-d') . '.log', 'a');

        $this->log("Initialized !");
    }


    /**
     *
     */
    private function configure()
    {
        $opts = getopt($this->shortopts, $this->longopts);

        if (isset($opts['no-cleanup'])) {
            $this->config['cleanUpOutputDir'] = false;
        }
        if (isset($opts['format'])) {
            $this->output_format = $opts['format'] !== '-' ? $opts['format'] : null;
        }
        if (isset($opts['output'])) {
            if (!is_dir($opts['output'])) {
                $dir = @mkdir($opts['output']);

                if ($dir === false) {
                    $this->log("Unable to get output dir.", self::LOG_LEVEL_FATAL);
                    $this->quit();
                }
            }

            $this->output_dir = $opts['output'];
        }
        if (isset($opts['input'])) {
            if (!is_dir($opts['input'])) {
                $this->log("Unable to get input dir.", self::LOG_LEVEL_FATAL);
                $this->quit();
            }

            $this->input_dir = $opts['input'];
        }
        if (isset($opts['quality'])) {
            $this->quality = $opts['quality'];
        }
    }


    /**
     *
     */
    private function cleanOutputDir()
    {
        $this->writeln("Cleaning output directory...");

        @array_map(
            'unlink',
            glob($this->output_dir . DIRECTORY_SEPARATOR . '*')
        );

        /* @array_map(
            'rmdir',
            glob($this->output_dir . DIRECTORY_SEPARATOR . '*')
        ); */

        $this->log(["Cleaned output directory", "Cleaned !"]);
    }


    /**
     *
     */
    private function discoverFiles()
    {
        $this->writeln("Discovering files...");

        $this->files = glob($this->input_dir . DIRECTORY_SEPARATOR . '*');

        $this->log("Found " . count($this->files) . " files !");

        foreach ($this->files as $file) {
            $fileLength = mb_strlen(pathinfo($file, PATHINFO_BASENAME));
            if ($fileLength > $this->max_file_length) {
                $this->max_file_length = $fileLength;
            }
        }

        if ($this->max_file_length > 100) {
            $this->max_file_length = 100;
        }
    }


    /**
     *
     */
    private function doProcess()
    {
        $this->writeln("Processing...");

        foreach ($this->files as $file) {
            $this->convertFile($file);
        }

        $this->log("Done !");
    }


    /**
     * @param string $input_file
     */
    private function convertFile(string $input_file)
    {
        $media        = $this->FFMpeg->open($input_file);
        $outputFormat = $this->output_format ?? $media->getStreams()->audios()->first()->get('codec_name');

        $fileinfo    = pathinfo($input_file);
//        $filename    = preg_replace("/( - YouTube){0,1}$/", '', $fileinfo['filename']);
        $filename    = $fileinfo['filename'];
        $output_file = $this->output_dir . DIRECTORY_SEPARATOR . $filename . '.' . self::OUTPUT_EXTS[$outputFormat];

        $this->initLineProcess($media, $filename);

        if (file_exists($output_file)) {
            $this->setLineState(self::STATE_SKIPPED);
            $this->log(["File $filename skipped."]);
            return;
        }

        $maxBitrate  = 4096;
        $maxChannels = 8;
        switch ($outputFormat) {
            case 'oga':
            case 'ogg':
                $format = new Vorbis();
                break;

            case 'flac':
                $format = new Flac();
                break;

            case 'mp3':
                $format      = new Mp3();
                $maxBitrate  = 320;
                $maxChannels = 4;
                break;

            case 'wav':
                $format = new Wav();
                break;

            case 'aac':
                $format = new Aac();
                break;

            case 'opus':
                $format = new Opus();
                break;

            default:
                $this->log(["Unsupported format.", "Unsupported format. Exiting."]);
                $this->quit();
        }

        $bitrate  = round($media->getStreams()->audios()->first()->get('bit_rate') / 1000, 0);
        $channels = $media->getStreams()->audios()->first()->get('channels');
        if ($bitrate > 0) {
            $format->setAudioKiloBitrate(min($bitrate, $maxBitrate));
        }
        if ($channels > 0) {
            $format->setAudioChannels(min($media->getStreams()->audios()->first()->get('channels'), $maxChannels));
        }

        try {
            $format->on('progress', function ($video, $format, $percentage) {
                $this->setLineState(self::STATE_PROGRESS, ['percentage' => $percentage]);
            });

            $media->save($format, $output_file);

            $this->setLineState(self::STATE_OK);

            $this->log(["File $filename processed."]);
        } catch (\Exception $e) {
            $this->log([$filename . " -- " . $e->getMessage() . ($e->getPrevious() ? " <<< " . $e->getPrevious()->getMessage() : "")], self::LOG_LEVEL_ERROR);

            $this->setLineState(self::STATE_ERROR);
            return;
        }
    }


    /**
     * @param string $message
     */
    private function writeln(string $message)
    {
        echo "\n\r$message";
    }

    /**
     * @param Audio|Video $media
     * @param string      $filename
     */
    private function initLineProcess($media, string $filename)
    {
        $codec = $media->getStreams()->audios()->first()->get('codec_name');
        $this->writeln(mb_substr($filename, 0, 100));

        $this->last_percentage_length = 0;

        echo str_repeat(" ", $this->max_file_length - mb_strlen($filename) + 2) .
            $codec . " -> ". $this->output_format . " " .
            str_repeat(" ", 6 - mb_strlen($codec));
    }

    /**
     * @param      $state
     * @param null $data
     */
    private function setLineState($state, $data = null)
    {
        switch ($state) {
            case self::STATE_SKIPPED:
                if ($this->last_percentage_length > 0) {
                    echo "\e[" . $this->last_percentage_length . "D";
                }
                echo "\e[0;37mSkipped\e[0m";
                break;

            case self::STATE_PROGRESS:
                $percentage = $data['percentage'];
                $length     = mb_strlen($percentage . '%');

                if ($this->last_percentage_length === 0) {
                    $this->last_percentage_length = $length;
                } else {
                    echo "\033[" . $length . "D";
                }

                if ($this->last_percentage_length < $length) {
                    echo str_repeat(" ", ($length - $this->last_percentage_length));
                    $this->last_percentage_length = $length;
                }

                echo "$percentage%";
                break;

            case self::STATE_OK:
                echo "\e[" . $this->last_percentage_length . "D";
                echo "\e[0;32mProcessed\e[0m";
                break;

            case self::STATE_ERROR:
                if ($this->last_percentage_length > 0) {
                    echo "\e[" . $this->last_percentage_length . "D";
                }
                echo "\e[0;31mError\e[0m";
                break;
        }
    }


    /**
     * @param        $message
     * @param string $level
     */
    private function log($message, $level = self::LOG_LEVEL_NOTICE)
    {
        $logMessage    = $message;
        $outputMessage = $message;
        if (is_array($message)) {
            $logMessage    = $message[0];
            $outputMessage = $message[1] ?? null;
        }

        $time    = (new \DateTime())->format('Y-m-d H:i:s');
        $message = "[$time] $level: \t$logMessage\n";

        fwrite($this->logFile, $message);

        if (!empty($outputMessage)) {
            $this->writeln($outputMessage);
        }
    }


    /**
     *
     */
    private function quit()
    {
        $this->log(['Process ended.']);

        echo "\n\r";
        exit;
    }
}
