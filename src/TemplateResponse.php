<?php

namespace AudioConverter;

class TemplateResponse extends Response
{
    public function __construct(string $template, array $parameters = [], int $httpCode = 200)
    {
        ob_start();

        foreach ($parameters as $parameter => $value) {
            $$parameter = $value;
        }
        require self::getTemplate($template);

        $body = ob_get_clean();

        parent::__construct($body, $httpCode);
    }

    /**
     * @param string $template
     *
     * @return string
     * @throws \Exception
     */
    private static function getTemplate(string $template)
    {
        global $templatesDir;

        $path = "{$templatesDir}/{$template}.html.php";
        if (file_exists($path)) {
            return $path;
        }

        throw new \Exception(sprintf("Template not found at %s", $path));
    }
}