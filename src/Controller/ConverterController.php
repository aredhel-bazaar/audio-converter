<?php

namespace AudioConverter\Controller;

use AudioConverter\TemplateResponse;

class ConverterController
{
    /**
     * @return false|string
     * @throws \Exception
     */
    public static function index()
    {
        global $projectDir;

        $inputGlobFiles  = glob("{$projectDir}/Resources/input/*");
        $outputGlobFiles = glob("{$projectDir}/Resources/output/*");
        $maxLines        = max($inputGlobFiles, $outputGlobFiles);

        $inputFiles = $outputFiles = [];

        foreach ($inputGlobFiles as $index => $file) {
            $inputFiles[sha1(pathinfo($file, PATHINFO_FILENAME))] = [
                'name'     => pathinfo($file, PATHINFO_FILENAME),
                'fullname' => pathinfo($file, PATHINFO_BASENAME),
                'size'     => filesize($file),
                'duration' => ''
            ];
        }

        foreach ($outputGlobFiles as $index => $file) {
            $outputFiles[sha1(pathinfo($file, PATHINFO_FILENAME))] = [
                'name'     => pathinfo($file, PATHINFO_FILENAME),
                'fullname' => pathinfo($file, PATHINFO_BASENAME),
                'size'     => filesize($file),
                'duration' => ''
            ];
        }

        return new TemplateResponse("index", [
            'inputFiles'  => $inputFiles,
            'outputFiles' => $outputFiles,
            'maxLines'    => $maxLines
        ]);
    }

    /**
     *
     */
    public static function convert()
    {
        global $projectDir;

        ini_set('zlib.output_compression', 0);
        ini_set('implicit_flush', 1);
        ini_set('output_buffering', 0);

        header("Content-Type: text/plain");
        header("Content-Encoding: identity");
        header("Transfer-Encoding: chunked");
        $proc = popen("php {$projectDir}/index.php --no-cleanup 2>&1", 'r');

        while( !feof($proc) ){
            echo fread($proc, 1024);
            flush(); // you have to flush buffer
        }
        fclose($proc);
    }
}