<?php

namespace AudioConverter\Extension\FFMpeg\Format\Audio;

use FFMpeg\Format\Audio\DefaultAudio;

class Aac extends DefaultAudio
{
    protected $audioCodec = 'aac';

    /**
     * {@inheritDoc}
     */
    public function getAvailableAudioCodecs()
    {
        return array($this->audioCodec);
    }
}