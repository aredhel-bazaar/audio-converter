<?php

namespace AudioConverter\Extension\FFMpeg\Format\Audio;

use FFMpeg\Format\Audio\DefaultAudio;

class Opus extends DefaultAudio
{
    protected $audioCodec = 'libopus';

    /**
     * {@inheritDoc}
     */
    public function getAvailableAudioCodecs()
    {
        return array($this->audioCodec);
    }
}