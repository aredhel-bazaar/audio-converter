<?php

include __DIR__."/../bootstrap.php";

/* Rudimentary routing system */
$routes = [
    '/'      => ['AudioConverter\\Controller\\ConverterController', 'index'],
    '/start' => ['AudioConverter\\Controller\\ConverterController', 'convert']
];

$path       = $_SERVER['REQUEST_URI'];
$controller = $routes[$path] ?? null;

if (isset($controller)) {
    try {
        $response = call_user_func_array($controller, []);

        if ($response instanceof \AudioConverter\Response) {
            $response->send();
        }
    } catch (Exception $e) {
        header(sprintf("X-Exception:%s", $e->getMessage()));
    }
}

if (!isset($response)) {
    header("HTTP/2 404 Not Found", true, 404);
    require __DIR__."/../templates/error/404.html.php";
}
