<?php

include __DIR__."/vendor/autoload.php";

spl_autoload_register(function($class) {
    $explodedClass = explode('\\', $class);
    array_shift($explodedClass);

    $classPath = __DIR__.DIRECTORY_SEPARATOR.'src'.DIRECTORY_SEPARATOR.implode(DIRECTORY_SEPARATOR, $explodedClass).'.php';
    if (file_exists($classPath)) {
        require_once  $classPath;
    }
});

$projectDir   = __DIR__;
$templatesDir = $projectDir . '/templates';